/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var container = 'container', //Div que contiene los feeds
    url = localStorage.getItem('feed-default-url') == null //Url por defecto para los feeds
            ? "http://www.elarcadefino.com/feed/" : localStorage.getItem('feed-default-url'),
    indice = 0, //Indice principal para ir viendo los enlaces
    entradas = [] //Entradas procesadas del feed elegido

//Capitaliza una cadena
function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//Muestra 5 entradas (a partir del indice)
function renderEntries(data) {
    document.getElementById(container).innerHTML = ('<h3>'+capitaliseFirstLetter(data.title)+'</h3>');
    var hasta = (indice + 5 < data.entries.length ) ? indice + 5 : data.entries.length;
    for(var i = indice ;i < hasta;i++) {
        var value = data.entries[i], titulo = value.title == "" ? "Sin titulo" : value.title;
        var thehtml = '<h3><a class="button-link" href="'+value.link+'">'+titulo+'</a></h3>';
        document.getElementById(container).innerHTML += thehtml;
    }
    var enlaces = document.querySelector('#' + container + ' a');
    //Los enlaces se abriran en el navegador o en una nueva pestaña(segun la plataforma)
    for(var i = 0; i < enlaces.length; i++) {
        enlaces[i].target = '_system';       
    }
}

//Se llama cada vez que se piden feeds. Se guardan los nuevos enlaces y se guardan
function initialize() {
    indice = 0;
    var feed = new google.feeds.Feed(url);
    feed.setNumEntries(10);
    feed.load(function(result) {
        if(!result.error) {
            var entries_prev = JSON.parse(localStorage.getItem('feed-entries-' + url))
                , insert = result.feed.entries;
            if(entries_prev != null) {
                insert = parseAll(joinIntersectObject(entries_prev.entries,insert));
            }
            localStorage.setItem('feed-entries-' + url, JSON.stringify({entries: insert}));
            entradas = {
                title: result.feed.title,
                entries: insert,
            }
            renderEntries(entradas);
        } 
    });
}

//Cargamos el feed por defecto y creamos eventos para pasar de pagina
function onReady(event) {
    if(document.readyState == 'complete') {
        var select_feeds = document.getElementById('feed-select').options;
        for(var i = 0;i< select_feeds.length; i++) {
            if(select_feeds[i].value == url) {
                select_feeds[i].selected = true;
            }
        }
        var feed_select = document.getElementById('feed-select');
        google.load("feeds", "1",{callback:initialize});
        feed_select.addEventListener('change', function() {
            url = this.value;
            try {
                google.load("feeds", "1",{callback:initialize});
            }
            catch(err) {
                navigator.notification.alert(
                    'No tienes internet, la aplicacion se cerrara'
                    , function() {
                        navigator.app.exit();
                      },
                    'Fail internet'
                );
            }
        })
        document.getElementById('feed-prev').addEventListener('click', function() {
            if ((indice - 5 >= 0)) {
                indice -= 5;
                renderEntries(entradas);
            }
            else if (indice > 0) {
                indice = 0;
                renderEntries(entradas);
            }
        })

        document.getElementById('feed-post').addEventListener('click', function() {
            var indices = entradas.entries.length;
            if ((indice + 5 < indices)) {
                indice += 5;
                renderEntries(entradas);
            }
        })

        document.getElementById('fav').addEventListener('click', function() {
            var select_fav = document.getElementById('feed-select');
            var url = select_fav[select_fav.selectedIndex].value;
            localStorage.setItem('feed-default-url', url);
        });
    } 
}

//Se unen las partes diferentes de dos arrays(se usa para guardar las nuevas entradas)
function joinIntersect(array1,array2) {
    var indexFirst = array1.indexOf(array2[0]);
    var array3 = [];
    if (indexFirst > 0) {
        array3 = array1.slice(0,indexFirst);
    }
    return array3.concat(array2);
}

/*Se unen las partes diferentes de dos arrays, donde sus elementos son objetos
    (se usa para guardar las nuevas entradas)*/
function joinIntersectObject(array1, array2) {
    array1 = stringyAll(array1);
    array2 = stringyAll(array2);
    return joinIntersect(array1,array2);
}

//Convierte cada elemento de un array en un JSON (solo si son objetos)
function stringyAll(array) {
    for (var i = 0; i < array.length; i++) {
        array[i] = JSON.stringify(array[i]);
    }
    return array;
}

function parseAll(array) {
    for (var i = 0; i < array.length; i++) {
        array[i] = JSON.parse(array[i]);
    }
    return array;
}